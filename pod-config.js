/**
 * @namespace
 * @property {string} AD_ID  - Element Id of the container in which video ad should play
 * @property {string} VIDEO_ID - Element Id of the <video> element that would play backup media
 * @property {boolean} FULLSCREEN - Indicates if the Ad Pod should cover the whole viewport
 * @property {number} [PADDING] - Padding (in pixels) on each side when the Ad Pod when in fullscreen, default to zero
 * @property {number} [WIDTH] - Width of the Ad Pod (in pixels), used when FULLSCREEN is false, defaults to 640
 * @property {number} [HEIGHT] - Height of the Ad Pod (in pixels), used when FULLSCREEN is false, defaults to 480
 * @property {number} MIN_DURATION - Minumum duration the Ad Pod runs.  The Ad Pod would stay live for atleast the minimum duration regardless of if ads are available or not, playing backup media instead
 * @property {number} MAX_DURATION - Maximum duration the Ad Pod runs - upon reaching this duration, the Ad Pod considers itself complete and invokes the callback function specified with CALLBACKFN
 * @property {number} MAX_ADS - Maximum number of Ads to show, the Ad Pod would consider itself complete when if it has played MAX_ADS eve if it has not reached the MAX_DURATION
 * @property {number} [DEBUG] - Indicates if severe messages should be shown on screen, default to false
 * @property {array} BACKUP_MEDIA - An array containing a list of paths to local media files to be played when no ad is available to be played
 * @property {function} CALLBACKFN - A callback function called by the Ad Pod upon completion 
*/
var Config = {
    AD_ID: 'adContainer',
    VIDEO_ID: 'videoPlayer',    
    FULLSCREEN: true,
    PADDING: 0,   
    WIDTH: 640,
    HEIGHT: 480,
    MIN_DURATION: 30,           
    MAX_DURATION: 90,           
    MAX_ADS: 9,
    DEBUG: false,                 
    BACKUP_MEDIA: [
      '08_SaveForEmergencies_Final.mp4',
      '06_NaturalGas_Final.mp4'
    ],
    CALLBACKFN: function(obj) {
      console.log("End of Play");
      console.log(obj);
    }
}