# Populus Ad Pod

## About

The Populus Ad Pod is a dynamic HTML application that connects to the Populus Marketplace and pulls video ads from a demand queue assigned to a network. It consists of a HTML file called *pod-local.html* and a locally linked JavaScript file called *pod-local.js* that works together to power third party advertising on top of existing video-based networks.  A unified version of the application that includes all the HTML and JavaScript in a single file called *pod-standalone.html* can also be used in lieu of two files.  The file config.js file is provided purely for documentation purposes only and is not used in any code samples.

### Other Files Included

Two other media files (.mp4) are provided as samples of backup media that the Ad Pod references.  The Ad Pod needs backup media when there's time available for it to show content and there are no ads avaialble in the demand queue.   These files can easily be replaced with any backup media of choice by the network by changing the configuration option called *BACKUP_MEDIA*

## Usage

The first step is to customize the HTML to fit needs of the network and / or to configure properties of the Ad Pod to adjust to a desired user experience.  A number of different configuration options are available for the networks to control.  These options could be changed directly on the HTML by altering the global called *Config*.  

### Config : <code>object</code>
**Kind**: global namespace  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| AD_ID | <code>string</code> | Element Id of the container in which video ad should play |
| VIDEO_ID | <code>string</code> | Element Id of the <video> element that would play backup media |
| FULLSCREEN | <code>boolean</code> | Indicates if the Ad Pod should cover the whole viewport |
| [PADDING] | <code>number</code> | Padding (in pixels) on each side when the Ad Pod when in fullscreen, default to zero |
| [WIDTH] | <code>number</code> | Width of the Ad Pod (in pixels), used when FULLSCREEN is false, defaults to 640 |
| [HEIGHT] | <code>number</code> | Height of the Ad Pod (in pixels), used when FULLSCREEN is false, defaults to 480 |
| MIN_DURATION | <code>number</code> | Minumum duration the Ad Pod runs.  The Ad Pod would stay live for atleast the minimum duration regardless of if ads are available or not, playing backup media instead |
| MAX_DURATION | <code>number</code> | Maximum duration the Ad Pod runs - upon reaching this duration, the Ad Pod considers itself complete and invokes the callback function specified with CALLBACKFN |
| MAX_ADS | <code>number</code> | Maximum number of Ads to show, the Ad Pod would consider itself complete when if it has played MAX_ADS eve if it has not reached the MAX_DURATION |
| [DEBUG] | <code>boolean</code> | Indicates if severe messages should be shown on screen, default to false |
| BACKUP_MEDIA | <code>array</code> | An array containing a list of paths to local media files to be played when no ad is available to be played |
| CALLBACKFN | <code>function</code> | A callback function called by the Ad Pod upon completion, make change here to notify your player |


### Installation 

Install by moving the customized HTML file and related assets (like JavaScript) to a suitable location on the player from where it could be triggered by the CMS on the video player

### Scheduling

The Ad Pod HTML content is meant to be scheduled just like any other static or dynamic HTML on existing Content Management Systems (CMS).  A key element of scheduling is to understanding how to obtain control back iahead of the scheduled end time of the Ad Pod.   Control can be passed from the Ad Pod back to the main video player by writing custom code in JavaScript in the property called CALLBACKFN and it generally varies by the CMS used.


Browser Specific Notes
----------------------

*Chromium*

Chrome version 66 and above blocks use of Autoplay on videos which could prevent 
the Ad Pod from playing ads.  It's recommended that when using Chromium based
players the policy AutoPlayAllowed be enabled.

To learn more about the policy visit [here](http://dev.chromium.org/administrators/policy-list-3#AutoplayAllowed)
